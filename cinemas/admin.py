from django.contrib import admin
from .models import Hall, Seance, Movie


class HallAdmin(admin.ModelAdmin):
    list_display = ('name', 'booked_chairs',)


admin.site.register(Hall, HallAdmin)
admin.site.register(Seance)
admin.site.register(Movie)

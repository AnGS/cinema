# posts/serializers.py
from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Hall, Seance, Movie


class HallSerializer(serializers.ModelSerializer):
    booked_chairs = serializers.ListField()

    class Meta:
        fields = ('id', 'name', 'booked_chairs')
        model = Hall


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name',)
        model = Movie


class SeanceSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'movies', 'start_date', 'end_date')
        model = Seance


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']

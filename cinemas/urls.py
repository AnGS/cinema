# posts/urls.py
from django.urls import path
from .views import MoviesList, MoviesDetail, HallsDetail, HallsList, SeanceList,SeanceDetail

urlpatterns = [
    path('movies/<int:pk>/', MoviesDetail.as_view()),
    path('movies/', MoviesList.as_view()),
    path('halls/<int:pk>/', HallsDetail.as_view()),
    path('halls/', HallsList.as_view()),
    path('seances/<int:pk>/', SeanceDetail.as_view()),
    path('seances/', SeanceList.as_view()),
]

from django.http import JsonResponse
from rest_framework import generics
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from .models import Hall, Movie, Seance
from .serializers import HallSerializer, MovieSerializer, SeanceSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User


class MoviesList(generics.ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MoviesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class HallsList(generics.ListCreateAPIView):
    queryset = Hall.objects.all()
    serializer_class = HallSerializer


class HallsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Hall.objects.all()
    serializer_class = HallSerializer


class SeanceList(generics.ListCreateAPIView):
    queryset = Seance.objects.all()
    serializer_class = SeanceSerializer


class SeanceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Seance.objects.all()
    serializer_class = SeanceSerializer


class ListHalls(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        names = [hall.name for hall in Hall.objects.all()]
        return Response(names)


class ListUsers(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        usernames = [user.username for user in User.objects.all()]
        return Response(usernames)


class ListSeances(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        names = [seance.name for seance in Seance.objects.all()]
        return Response(names)


class ListMovies(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        names = [movie.name for movie in Movie.objects.all()]
        return Response(names)


class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_Id': user.pk,
            'email': user.email
        })


@property
def allowed_methods(self):
    """
    Return the list of allowed HTTP methods, uppercased.
    """
    self.http_method_names.append("patch")
    return [method.upper() for method in self.http_method_names
            if hasattr(self, method)]


class BookView(APIView):
    def get_object(self, pk):
        return Hall.objects.get(pk=pk)

    def patch(self, request, pk):
        testmodel_object = self.get_object(pk)
        booked_data = testmodel_object.booked_chairs
        serializer = HallSerializer(testmodel_object, data=request.data,
                                    partial=True)
        if serializer.is_valid() and not list(set(request.data['booked_chairs']) & set(list(booked_data))):
            serializer.save()
            return JsonResponse(data=serializer.data, status=201, safe=False)
        return JsonResponse(data="This chairs are booked", status=400, safe=False)
